import os
from typing import Dict

from fwutil.FileWriter import open_file
from fwutil.WriteMode import WriteMode

from generator.extension.ExtensionFunctionClassGenerator import ExtensionFunctionClassGenerator
from generator.extension.clazz.JsonArrayPrimitiveExtGenerator import JsonArrayPrimitiveExtGenerator
from generator.extension.clazz.JsonElementPrimitiveExtGenerator import JsonElementPrimitiveExtGenerator
from generator.extension.clazz.JsonObjectPrimitiveExtGenerator import JsonObjectPrimitiveExtGenerator
from generator.extension.clazz.JsonObjectPrimitiveMapExtGenerator import JsonObjectPrimitiveMapExtGenerator

cwd = os.getcwd()
gson_kt_extensions_dir = os.path.abspath(os.path.join(cwd, os.pardir))
source_path = os.path.join(gson_kt_extensions_dir, "core", "src", "main", "kotlin")

extension_path = os.path.join(source_path, "fe", "gson", "extension")
json_extensions_path = os.path.join(extension_path, "json")

generators: Dict[ExtensionFunctionClassGenerator, str] = {
    JsonArrayPrimitiveExtGenerator(): json_extensions_path,
    JsonElementPrimitiveExtGenerator(): json_extensions_path,
    JsonObjectPrimitiveExtGenerator(): json_extensions_path,
    JsonObjectPrimitiveMapExtGenerator(): json_extensions_path,
}

for generator, path in generators.items():
    file = os.path.join(path, generator.package_name.replace("`", ""), generator.file_name)
    with open_file(file, WriteMode.WRITE) as fw:
        generator.write(fw)
