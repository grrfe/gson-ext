from abc import ABC, abstractmethod
from typing import List

from fwutil.FileWriter import FileWriter

from datatype.KtDataType import KtDataType, KT_DATA_TYPES
from datatype.KtException import KtException


class ExtensionFunctionClassGenerator(ABC):
    __BASE_PACKAGE = "fe.gson.extension.json"
    __IMPLICIT_IMPORTS = ["java.math.BigDecimal", "java.math.BigInteger"]

    def __init__(self, file_name: str, package_name: str, imports: List[str], default_exceptions: List[KtException]):
        self.file_name = file_name
        self.package_name = package_name
        self.imports = imports
        self.default_exceptions = default_exceptions

    @abstractmethod
    def _write_data_type(self, writer: FileWriter, data_type: KtDataType, throws_annotation: str):
        pass

    def _write_package_imports(self, writer: FileWriter):
        writer.write_line(f"package {self.__BASE_PACKAGE}.{self.package_name}", new_lines=2)
        for i in self.imports + self.__IMPLICIT_IMPORTS:
            writer.write_line(f"import {i}", new_lines=1)

    def __generate_throws_annotation(self, exception: KtException) -> str:
        exceptions = self.default_exceptions[:]
        exceptions.append(exception)

        return f'@Throws({", ".join([exception.value for exception in exceptions])})'

    def write(self, writer: FileWriter):
        self._write_package_imports(writer)
        for data_type in KT_DATA_TYPES:
            writer.new_line()

            throws_annotation = self.__generate_throws_annotation(data_type.exception)
            self._write_data_type(writer, data_type, throws_annotation)
