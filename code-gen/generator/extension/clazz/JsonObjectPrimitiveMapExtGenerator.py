from datatype.KtDataType import KtDataType
from datatype.KtException import KtException
from generator.extension.ExtensionFunctionClassGenerator import ExtensionFunctionClassGenerator
from fwutil.FileWriter import FileWriter


class JsonObjectPrimitiveMapExtGenerator(ExtensionFunctionClassGenerator):
    def __init__(self):
        super().__init__("JsonObjectPrimitiveMapExt.kt", "`object`", [
            "com.google.gson.JsonObject",
            "fe.gson.extension.json.element.*"
        ], [KtException.ILLEGAL_STATE_EXCEPTION])

    def _write_data_type(self, writer: FileWriter, data_type: KtDataType, throws_annotation: str):
        writer.write_multiline(f"""
            {throws_annotation}
            public fun JsonObject.{data_type.ext_fun_name}Map(): Map<String, {data_type.type_name}> = map {{ it.{data_type.ext_fun_name}() }}
            public fun JsonObject.{data_type.ext_fun_name}OrNullMap(keepNulls: Boolean = true): Map<String, {data_type.type_name_nullable}> = mapWithNulls(keepNulls) {{ it.{data_type.ext_fun_name}OrNull() }}
        """)
