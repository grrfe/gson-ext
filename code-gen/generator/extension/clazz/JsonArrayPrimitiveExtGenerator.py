from datatype.KtDataType import KtDataType
from datatype.KtException import KtException
from generator.extension.ExtensionFunctionClassGenerator import ExtensionFunctionClassGenerator
from fwutil.FileWriter import FileWriter


class JsonArrayPrimitiveExtGenerator(ExtensionFunctionClassGenerator):

    def __init__(self):
        super().__init__("JsonArrayPrimitiveExt.kt", "array", [
            "com.google.gson.JsonArray",
            "fe.gson.extension.json.element.*"
        ], [KtException.ILLEGAL_STATE_EXCEPTION])

    def _write_data_type(self, writer: FileWriter, data_type: KtDataType, throws_annotation: str):
        writer.write_multiline(f"""
            {throws_annotation}
            public fun JsonArray.{data_type.ext_fun_name_plural}(): List<{data_type.type_name}> = map {{ it.{data_type.ext_fun_name}() }}
            public fun JsonArray.{data_type.ext_fun_name_plural}OrNull(keepNulls: Boolean = true): List<{data_type.type_name_nullable}> = mapWithNulls(keepNulls) {{ it.{data_type.ext_fun_name}OrNull() }}
        """)
