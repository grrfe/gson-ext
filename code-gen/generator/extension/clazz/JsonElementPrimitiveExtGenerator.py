from datatype.KtDataType import KtDataType
from datatype.KtException import KtException
from generator.extension.ExtensionFunctionClassGenerator import ExtensionFunctionClassGenerator
from fwutil.FileWriter import FileWriter


class JsonElementPrimitiveExtGenerator(ExtensionFunctionClassGenerator):

    def __init__(self):
        super().__init__("JsonElementPrimitiveExt.kt", "element", ["com.google.gson.JsonElement"],
                         [KtException.ILLEGAL_STATE_EXCEPTION])

    def _write_data_type(self, writer: FileWriter, data_type: KtDataType, throws_annotation: str):
        writer.write_multiline(f"""
            {throws_annotation}
            public fun JsonElement.{data_type.ext_fun_name}(): {data_type.type_name} = primitive().as{data_type.type_name}
            public fun JsonElement.{data_type.ext_fun_name}OrNull(): {data_type.type_name_nullable} = primitiveMappedOrNull {{ it.as{data_type.type_name} }}
        """)
