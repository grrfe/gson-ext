from datatype.KtDataType import KtDataType
from datatype.KtException import KtException
from generator.extension.ExtensionFunctionClassGenerator import ExtensionFunctionClassGenerator
from fwutil.FileWriter import FileWriter


class JsonObjectPrimitiveExtGenerator(ExtensionFunctionClassGenerator):
    def __init__(self):
        super().__init__("JsonObjectPrimitiveExt.kt", "`object`", [
            "com.google.gson.JsonObject",
            "fe.gson.exception.NoSuchKeyInElementFoundException"
        ], [KtException.CLASS_CAST_EXCEPTION, KtException.NO_SUCH_KEY_IN_ELEMENT_FOUND_EXCEPTION])

    def _write_data_type(self, writer: FileWriter, data_type: KtDataType, throws_annotation: str):
        writer.write_multiline(f"""
            {throws_annotation}
            public fun JsonObject.{data_type.gson_type}(key: String): {data_type.type_name} = asPrimitive(key).{data_type.gson_type}
            public fun JsonObject.{data_type.gson_type}OrNull(key: String): {data_type.type_name_nullable} = asPrimitiveMappedOrNull(key) {{ it.{data_type.gson_type} }}
        """)
