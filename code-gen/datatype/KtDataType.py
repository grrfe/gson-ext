from abc import ABC

from datatype.KtException import KtException


class KtDataType(ABC):

    def __init__(self, type_name: str, ext_fun_name: str, exception: KtException):
        self.type_name = type_name
        self.type_name_nullable = f"{type_name}?"
        self.ext_fun_name = ext_fun_name
        self.ext_fun_name_plural = f"{ext_fun_name}s"
        self.exception = exception
        self.as_nullable_list = f"List<{self.type_name_nullable}>"
        self.gson_type = f"as{type_name}"


class KtString(KtDataType):
    def __init__(self):
        super(KtString, self).__init__("String", "string", KtException.ASSERTION_ERROR)


class KtBoolean(KtDataType):
    def __init__(self):
        super(KtBoolean, self).__init__("Boolean", "boolean", KtException.ASSERTION_ERROR)


class KtLong(KtDataType):
    def __init__(self):
        super(KtLong, self).__init__("Long", "long", KtException.NUMBER_FORMAT_EXCEPTION)


class KtInt(KtDataType):
    def __init__(self):
        super(KtInt, self).__init__("Int", "int", KtException.NUMBER_FORMAT_EXCEPTION)


class KtFloat(KtDataType):
    def __init__(self):
        super(KtFloat, self).__init__("Float", "float", KtException.NUMBER_FORMAT_EXCEPTION)


class KtDouble(KtDataType):
    def __init__(self):
        super(KtDouble, self).__init__("Double", "double", KtException.NUMBER_FORMAT_EXCEPTION)


class KtShort(KtDataType):
    def __init__(self):
        super(KtShort, self).__init__("Short", "short", KtException.NUMBER_FORMAT_EXCEPTION)


class KtByte(KtDataType):
    def __init__(self):
        super(KtByte, self).__init__("Byte", "byte", KtException.NUMBER_FORMAT_EXCEPTION)


class KtBigDecimal(KtDataType):
    def __init__(self):
        super(KtBigDecimal, self).__init__("BigDecimal", "bigDecimal", KtException.NUMBER_FORMAT_EXCEPTION)


class KtBigInt(KtDataType):
    def __init__(self):
        super(KtBigInt, self).__init__("BigInteger", "bigInt", KtException.NUMBER_FORMAT_EXCEPTION)


KT_DATA_TYPES = [
    KtString(),
    KtBoolean(),
    KtInt(),
    KtLong(),
    KtDouble(),
    KtFloat(),
    KtShort(),
    KtByte(),
    KtBigDecimal(),
    KtBigInt()
]
