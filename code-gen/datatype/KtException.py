from enum import Enum


class KtException(Enum):
    ASSERTION_ERROR = "AssertionError::class"
    NUMBER_FORMAT_EXCEPTION = "NumberFormatException::class"
    ILLEGAL_STATE_EXCEPTION = "IllegalStateException::class"
    CLASS_CAST_EXCEPTION = "ClassCastException::class"
    NO_SUCH_KEY_IN_ELEMENT_FOUND_EXCEPTION = "NoSuchKeyInElementFoundException::class"
