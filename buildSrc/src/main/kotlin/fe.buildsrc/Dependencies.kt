package fe.buildsrc

import net.nemerosa.versioning.VersioningExtension

class Dependency(val name: String, val major: Int) {
    fun with(minor: Int, patch: Int): String {
        return "$major.$minor.$patch"
    }

    override fun toString(): String {
        return "$name$major"
    }
}

object Dependencies {
    val koin = Dependency("koin", 4)
    val gson = Dependency("gson", 2)

    private fun joinDependencies(dependencies: Array<out Dependency>): String {
        return dependencies.sortedBy { it.name }.joinToString("-")
    }

    private val allDependencies = arrayOf(koin, gson)

    fun with(versioning: VersioningExtension, vararg dependencies: Dependency = allDependencies): String {
        return "${versioning.simpleSemver()}-${joinDependencies(dependencies)}"
    }
}

private fun VersioningExtension.simpleSemver(): String {
    if (info.tag != null) {
        val idx = info.tag.indexOf("-")
        val semver = if (idx != -1) {
            info.tag.substring(0, idx)
        } else info.tag

        return semver
    }

    return info.full
}
