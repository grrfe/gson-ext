pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
        maven { url = uri("https://jitpack.io") }
    }

    plugins {
        id("de.fayard.refreshVersions") version "0.60.5"
        id("net.nemerosa.versioning") version "3.1.0"
        kotlin("jvm") version "2.1.0" apply false
    }
}

@Suppress("UnstableApiUsage")
dependencyResolutionManagement {
    repositories {
        google()
        mavenCentral()
        maven(url = "https://jitpack.io")
        gradlePluginPortal()
    }
}

plugins {
    id("de.fayard.refreshVersions")
}

rootProject.name = "gson-ext"
include(":core", ":koin")

fun hasEnv(name: String): Boolean {
    return System.getenv(name)?.toBooleanStrictOrNull() == true
}

val isCI = hasEnv("CI")
val isJitPack = hasEnv("JITPACK")

if (!isJitPack) {
}
