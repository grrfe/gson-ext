import fe.buildsrc.Dependencies

plugins {
    kotlin("jvm")
    `java-library`
    `maven-publish`
    id("net.nemerosa.versioning")
}

dependencies {
    api(project(":core"))
    api(Koin.core)

    testImplementation(kotlin("test"))
}

kotlin {
    jvmToolchain(17)
    explicitApi()
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = project.group.toString()
            artifactId = "koin"
            version =  Dependencies.with(versioning, Dependencies.koin)

            from(components["java"])
        }
    }
}
