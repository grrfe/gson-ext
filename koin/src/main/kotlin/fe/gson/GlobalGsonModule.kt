package fe.gson

import com.google.gson.Gson
import fe.gson.context.GlobalGsonContext
import fe.gson.context.ObjectConverter
import org.koin.core.module.Module
import org.koin.dsl.module

public val globalGsonModule: Module = module {
    single<Gson> { GlobalGsonContext.gson }
}

public val globalObjectConverter: Module = module {
    single<ObjectConverter> { GlobalGsonContext.converter }
}
