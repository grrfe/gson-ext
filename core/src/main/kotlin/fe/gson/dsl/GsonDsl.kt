package fe.gson.dsl
@DslMarker
@Target(AnnotationTarget.CLASS)
internal annotation class GsonDsl
