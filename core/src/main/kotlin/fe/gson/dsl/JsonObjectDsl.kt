package fe.gson.dsl

import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import fe.gson.context.GlobalGsonContext
import fe.gson.context.IJsonElement

public typealias JsonObjectDslInit = JsonObjectDsl.() -> Unit
public typealias JsonObjectDslInitData<T> = JsonObjectDsl.(T) -> Unit


public inline fun <T> JsonObject.initWith(data: T, init: JsonObjectDslInitData<T>): JsonObject {
    val dsl = JsonObjectDsl(this)
    dsl.init(data)

    return this
}

public inline fun JsonObject.initWith(init: JsonObjectDslInit): JsonObject {
    JsonObjectDsl(this).apply(init)
    return this
}

public fun JsonObjectDslInit.new(): JsonObject {
    return JsonObject().initWith(this)
}

public fun <T> JsonObjectDslInitData<T>.new(data: T): JsonObject {
    return JsonObject().initWith(data, this)
}

public inline fun jsonObject(jsonObject: JsonObject = JsonObject(), init: JsonObjectDslInit): JsonObject {
    return jsonObject.initWith(init)
}

public inline fun lazyJsonObject(
    jsonObject: JsonObject = JsonObject(),
    mode: LazyThreadSafetyMode = LazyThreadSafetyMode.SYNCHRONIZED,
    crossinline init: JsonObjectDslInit,
): Lazy<JsonObject> {
    return lazy(mode = mode) { jsonObject(jsonObject, init) }
}


@GsonDsl
@JvmInline
public value class JsonObjectDsl(private val obj: JsonObject = JsonObject()) : IJsonElement {
    public override fun toJsonElement(): JsonElement {
        return obj
    }

    public operator fun String.plusAssign(any: Any?) {
        val elem = GlobalGsonContext.converter.toJsonElement(any)
        if (elem != null) obj.add(this, elem)
    }

    public operator fun String.plusAssign(element: JsonElement) {
        obj.add(this, element)
    }

    public operator fun String.plusAssign(element: String) {
        obj.addProperty(this, element)
    }

    public operator fun String.plusAssign(element: Boolean) {
        obj.addProperty(this, element)
    }

    public operator fun String.plusAssign(element: Char) {
        obj.addProperty(this, element)
    }

    public operator fun String.plusAssign(element: Number) {
        obj.addProperty(this, element)
    }

    public operator fun rangeTo(obj: JsonObject) {
        for ((key, element) in obj.asMap()) this.obj.add(key, element)
    }

    public operator fun get(vararg items: Any?): JsonArray {
        return jsonArray(*items)
    }
}
