package fe.gson.dsl

import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonPrimitive
import fe.gson.context.IJsonElement
import fe.gson.extension.json.array.addIfJsonElement

public typealias JsonArrayDslInit = JsonArrayDsl.() -> Unit

public inline fun JsonArray.initWith(init: JsonArrayDslInit): JsonArray {
    JsonArrayDsl(this).apply(init)
    return this
}

public fun JsonArrayDslInit.new(): JsonArray {
    return JsonArray().initWith(this)
}

public inline fun jsonArray(jsonArray: JsonArray = JsonArray(), init: JsonArrayDslInit): JsonArray {
    return jsonArray.initWith(init)
}

public fun jsonArray(vararg items: Any?): JsonArray {
    return JsonArrayDsl().add(items.toMutableList())
}

public inline fun lazyJsonArray(
    jsonArray: JsonArray = JsonArray(),
    mode: LazyThreadSafetyMode = LazyThreadSafetyMode.SYNCHRONIZED,
    crossinline init: JsonArrayDslInit,
): Lazy<JsonArray> {
    return lazy(mode = mode) { jsonArray(jsonArray, init) }
}

@GsonDsl
@JvmInline
public value class NumberValue(private val value: Number) : IJsonElement {
    public override fun toJsonElement(): JsonElement {
        return JsonPrimitive(value)
    }
}

@GsonDsl
@JvmInline
public value class JsonArrayDsl(private val array: JsonArray = JsonArray()) : IJsonElement {
    public override fun toJsonElement(): JsonElement {
        return array
    }

    public fun add(vararg items: Any?): JsonArray {
        for (item in items) array.addIfJsonElement(item)
        return array
    }

    public fun add(items: Iterable<Any?>): JsonArray {
        for (item in items) array.addIfJsonElement(item)
        return array
    }

    public operator fun JsonElement.unaryPlus(): JsonArray {
        array.add(this)
        return array
    }

    public operator fun Any?.unaryPlus(): JsonArray {
        return array.addIfJsonElement(this)
    }

    public operator fun NumberValue.unaryPlus(): JsonArray {
        this@JsonArrayDsl.array.add(toJsonElement())
        return this@JsonArrayDsl.array
    }

    public operator fun String.unaryPlus(): JsonArray {
        array.add(this)
        return array
    }

    public operator fun Boolean.unaryPlus(): JsonArray {
        array.add(this)
        return array
    }

    public operator fun Char.unaryPlus(): JsonArray {
        array.add(this)
        return array
    }

    public operator fun Number.unaryPlus(): JsonArray {
        array.add(this)
        return array
    }

    public fun n(number: Number): NumberValue {
        return NumberValue(number)
    }

    public operator fun rangeTo(array: JsonArray): JsonArray {
        array.addAll(array)
        return array
    }

//    public operator fun get(vararg items: Any?): JsonArray {
//        return jsonArray(*items)
//    }
}
