package fe.gson.util

import com.google.gson.JsonArray
import fe.gson.dsl.JsonArrayDsl
import fe.gson.dsl.JsonObjectDslInit
import fe.gson.dsl.jsonObject
import fe.gson.extension.json.array.addIfJsonElement

public fun jsonArrayItems(vararg items: Any?): JsonArray {
    val array = JsonArray()
    for (item in items) array.addIfJsonElement(item)

    return array
}

public fun jsonArrayItems(items: Iterable<Any?>): JsonArray {
    val array = JsonArray()
    for (item in items) array.addIfJsonElement(item)

    return array
}

public inline fun jsonArrayWithSingleJsonObject(init: JsonObjectDslInit): JsonArray {
    val array = JsonArray()
    array.add(jsonObject(init = init))

    return array
}
