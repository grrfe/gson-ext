package fe.gson.util

import com.google.gson.*
import fe.gson.JsonReader
import fe.gson.extension.writeJson
import fe.gson.context.GlobalGsonContext
import java.io.Reader
import java.io.Writer

public object Json {
    public val jsonReader: JsonReader = JsonReader()
    public val jsonParser: fe.gson.JsonParser = fe.gson.JsonParser()

    @Throws(JsonParseException::class, ClassCastException::class)
    public inline fun <reified T : JsonElement> parseJson(reader: Reader): T {
        return jsonParser.parseJson(reader)
    }

    @Throws(JsonParseException::class, ClassCastException::class)
    public inline fun <reified T : JsonElement> parseJson(string: String): T {
        return jsonParser.parseJson(string)
    }

    public inline fun <reified T : JsonElement> parseJsonOrNull(reader: Reader): T? {
        return jsonParser.parseJsonOrNull(reader)
    }

    public inline fun <reified T : JsonElement> parseJsonOrNull(string: String): T? {
        return jsonParser.parseJsonOrNull(string)
    }

    @Throws(JsonParseException::class, ClassCastException::class)
    public inline fun <reified T> fromJson(reader: Reader): T {
        return jsonReader.fromJson(reader)
    }

    @Throws(JsonParseException::class, ClassCastException::class)
    public inline fun <reified T> fromJson(string: String): T {
        return jsonReader.fromJson(string)
    }

    public inline fun <reified T> fromJsonOrNull(reader: Reader): T? {
        return jsonReader.fromJsonOrNull(reader)
    }

    public inline fun <reified T> fromJsonOrNull(string: String): T? {
        return jsonReader.fromJsonOrNull(string)
    }

    @Throws(JsonIOException::class)
    public inline fun <reified T : JsonElement> toJson(
        writer: Writer,
        element: T,
        gson: Gson = GlobalGsonContext.gson,
    ) {
        writer.use { gson.writeJson<T>(element, writer) }
    }

    @Throws(JsonIOException::class)
    public fun toJson(
        writer: Writer,
        src: Any,
        gson: Gson = GlobalGsonContext.gson,
    ) {
        writer.use { gson.writeJson(src, writer) }
    }
}
