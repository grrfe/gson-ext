package fe.gson.extension.io

import com.google.gson.*
import fe.gson.util.*
import java.io.File

@Throws(JsonParseException::class, ClassCastException::class)
public inline fun <reified T : JsonElement> File.parseJson(): T = Json.parseJson(bufferedReader())
public inline fun <reified T : JsonElement> File.parseJsonOrNull(): T? = Json.parseJsonOrNull(bufferedReader())

@Throws(JsonParseException::class, ClassCastException::class)
public inline fun <reified T> File.fromJson(): T = Json.fromJson<T>(bufferedReader())

public inline fun <reified T> File.fromJsonOrNull(): T? = Json.fromJsonOrNull<T>(bufferedReader())

@Throws(JsonIOException::class)
public inline fun <reified T : JsonElement> File.toJson(element: T): Unit = Json.toJson<T>(bufferedWriter(), element)

@Throws(JsonIOException::class)
public fun File.toJson(src: Any): Unit = Json.toJson(bufferedWriter(), src)
