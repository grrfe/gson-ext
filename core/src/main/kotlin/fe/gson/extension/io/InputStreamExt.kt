package fe.gson.extension.io

import com.google.gson.*
import fe.gson.util.Json
import java.io.InputStream
import java.io.OutputStream

@Throws(JsonParseException::class, ClassCastException::class)
public inline fun <reified T : JsonElement> InputStream.parseJson(): T = Json.parseJson(bufferedReader())
public inline fun <reified T : JsonElement> InputStream.parseJsonOrNull(): T? = Json.parseJsonOrNull(bufferedReader())

@Throws(JsonParseException::class, ClassCastException::class)
public inline fun <reified T> InputStream.fromJson(): T = Json.fromJson<T>(bufferedReader())

public inline fun <reified T> InputStream.fromJsonOrNull(): T? = Json.fromJsonOrNull<T>(bufferedReader())

@Throws(JsonIOException::class)
public inline fun <reified T : JsonElement> OutputStream.toJson(element: T): Unit = Json.toJson<T>(bufferedWriter(), element)

@Throws(JsonIOException::class)
public fun OutputStream.toJson(src: Any): Unit = Json.toJson(bufferedWriter(), src)
