package fe.gson.extension.io

import com.google.gson.JsonElement
import com.google.gson.JsonIOException
import com.google.gson.JsonParseException
import fe.gson.util.Json
import java.nio.file.Path
import kotlin.io.path.bufferedReader
import kotlin.io.path.bufferedWriter

@Throws(JsonParseException::class, ClassCastException::class)
public inline fun <reified T : JsonElement> Path.parseJson(): T {
    return Json.parseJson(bufferedReader())
}
public inline fun <reified T : JsonElement> Path.parseJsonOrNull(): T? {
    return Json.parseJsonOrNull(bufferedReader())
}

@Throws(JsonParseException::class, ClassCastException::class)
public inline fun <reified T> Path.fromJson(): T {
    return Json.fromJson<T>(bufferedReader())
}

public inline fun <reified T> Path.fromJsonOrNull(): T? {
    return Json.fromJsonOrNull<T>(bufferedReader())
}

@Throws(JsonIOException::class)
public inline fun <reified T : JsonElement> Path.toJson(element: T) {
    Json.toJson<T>(bufferedWriter(), element)
}

@Throws(JsonIOException::class)
public fun Path.toJson(src: Any) {
    Json.toJson(bufferedWriter(), src)
}
