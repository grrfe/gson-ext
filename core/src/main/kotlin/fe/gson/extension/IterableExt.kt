package fe.gson.extension

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import fe.gson.dsl.JsonObjectDslInitData
import fe.gson.dsl.initWith

public inline fun <T> Iterable<T>.mapToJsonObjectArray(init: JsonObjectDslInitData<T>): JsonArray {
    val jsonArray = JsonArray()
    for (element in this) jsonArray.add(JsonObject().initWith(element, init))

    return jsonArray
}
