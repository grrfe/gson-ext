package fe.gson.extension.json.`object`

import com.google.gson.JsonObject
import fe.gson.extension.json.element.*
import java.math.BigDecimal
import java.math.BigInteger


@Throws(IllegalStateException::class, AssertionError::class)
public fun JsonObject.stringMap(): Map<String, String> = map { it.string() }
public fun JsonObject.stringOrNullMap(keepNulls: Boolean = true): Map<String, String?> = mapWithNulls(keepNulls) { it.stringOrNull() }

@Throws(IllegalStateException::class, AssertionError::class)
public fun JsonObject.booleanMap(): Map<String, Boolean> = map { it.boolean() }
public fun JsonObject.booleanOrNullMap(keepNulls: Boolean = true): Map<String, Boolean?> = mapWithNulls(keepNulls) { it.booleanOrNull() }

@Throws(IllegalStateException::class, NumberFormatException::class)
public fun JsonObject.intMap(): Map<String, Int> = map { it.int() }
public fun JsonObject.intOrNullMap(keepNulls: Boolean = true): Map<String, Int?> = mapWithNulls(keepNulls) { it.intOrNull() }

@Throws(IllegalStateException::class, NumberFormatException::class)
public fun JsonObject.longMap(): Map<String, Long> = map { it.long() }
public fun JsonObject.longOrNullMap(keepNulls: Boolean = true): Map<String, Long?> = mapWithNulls(keepNulls) { it.longOrNull() }

@Throws(IllegalStateException::class, NumberFormatException::class)
public fun JsonObject.doubleMap(): Map<String, Double> = map { it.double() }
public fun JsonObject.doubleOrNullMap(keepNulls: Boolean = true): Map<String, Double?> = mapWithNulls(keepNulls) { it.doubleOrNull() }

@Throws(IllegalStateException::class, NumberFormatException::class)
public fun JsonObject.floatMap(): Map<String, Float> = map { it.float() }
public fun JsonObject.floatOrNullMap(keepNulls: Boolean = true): Map<String, Float?> = mapWithNulls(keepNulls) { it.floatOrNull() }

@Throws(IllegalStateException::class, NumberFormatException::class)
public fun JsonObject.shortMap(): Map<String, Short> = map { it.short() }
public fun JsonObject.shortOrNullMap(keepNulls: Boolean = true): Map<String, Short?> = mapWithNulls(keepNulls) { it.shortOrNull() }

@Throws(IllegalStateException::class, NumberFormatException::class)
public fun JsonObject.byteMap(): Map<String, Byte> = map { it.byte() }
public fun JsonObject.byteOrNullMap(keepNulls: Boolean = true): Map<String, Byte?> = mapWithNulls(keepNulls) { it.byteOrNull() }

@Throws(IllegalStateException::class, NumberFormatException::class)
public fun JsonObject.bigDecimalMap(): Map<String, BigDecimal> = map { it.bigDecimal() }
public fun JsonObject.bigDecimalOrNullMap(keepNulls: Boolean = true): Map<String, BigDecimal?> = mapWithNulls(keepNulls) { it.bigDecimalOrNull() }

@Throws(IllegalStateException::class, NumberFormatException::class)
public fun JsonObject.bigIntMap(): Map<String, BigInteger> = map { it.bigInt() }
public fun JsonObject.bigIntOrNullMap(keepNulls: Boolean = true): Map<String, BigInteger?> = mapWithNulls(keepNulls) { it.bigIntOrNull() }