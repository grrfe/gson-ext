package fe.gson.extension.json.element

import com.google.gson.JsonElement
import java.math.BigDecimal
import java.math.BigInteger


@Throws(IllegalStateException::class, AssertionError::class)
public fun JsonElement.string(): String = primitive().asString
public fun JsonElement.stringOrNull(): String? = primitiveMappedOrNull { it.asString }

@Throws(IllegalStateException::class, AssertionError::class)
public fun JsonElement.boolean(): Boolean = primitive().asBoolean
public fun JsonElement.booleanOrNull(): Boolean? = primitiveMappedOrNull { it.asBoolean }

@Throws(IllegalStateException::class, NumberFormatException::class)
public fun JsonElement.int(): Int = primitive().asInt
public fun JsonElement.intOrNull(): Int? = primitiveMappedOrNull { it.asInt }

@Throws(IllegalStateException::class, NumberFormatException::class)
public fun JsonElement.long(): Long = primitive().asLong
public fun JsonElement.longOrNull(): Long? = primitiveMappedOrNull { it.asLong }

@Throws(IllegalStateException::class, NumberFormatException::class)
public fun JsonElement.double(): Double = primitive().asDouble
public fun JsonElement.doubleOrNull(): Double? = primitiveMappedOrNull { it.asDouble }

@Throws(IllegalStateException::class, NumberFormatException::class)
public fun JsonElement.float(): Float = primitive().asFloat
public fun JsonElement.floatOrNull(): Float? = primitiveMappedOrNull { it.asFloat }

@Throws(IllegalStateException::class, NumberFormatException::class)
public fun JsonElement.short(): Short = primitive().asShort
public fun JsonElement.shortOrNull(): Short? = primitiveMappedOrNull { it.asShort }

@Throws(IllegalStateException::class, NumberFormatException::class)
public fun JsonElement.byte(): Byte = primitive().asByte
public fun JsonElement.byteOrNull(): Byte? = primitiveMappedOrNull { it.asByte }

@Throws(IllegalStateException::class, NumberFormatException::class)
public fun JsonElement.bigDecimal(): BigDecimal = primitive().asBigDecimal
public fun JsonElement.bigDecimalOrNull(): BigDecimal? = primitiveMappedOrNull { it.asBigDecimal }

@Throws(IllegalStateException::class, NumberFormatException::class)
public fun JsonElement.bigInt(): BigInteger = primitive().asBigInteger
public fun JsonElement.bigIntOrNull(): BigInteger? = primitiveMappedOrNull { it.asBigInteger }