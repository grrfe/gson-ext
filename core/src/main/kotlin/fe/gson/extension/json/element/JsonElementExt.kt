package fe.gson.extension.json.element

import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonPrimitive

public inline fun <T> JsonElement.primitiveMappedOrNull(fn: (JsonPrimitive) -> T): T? {
    val primitive = primitiveOrNull() ?: return null
    return try {
        fn(primitive)
    } catch (e: Throwable) {
        null
    }
}

@Throws(IllegalStateException::class)
public fun JsonElement.primitive(): JsonPrimitive = asJsonPrimitive
public fun JsonElement.primitiveOrNull(): JsonPrimitive? = try {
    asJsonPrimitive
} catch (e: Throwable) {
    null
}


@Throws(IllegalStateException::class)
public fun JsonElement.`object`(): JsonObject = asJsonObject
public fun JsonElement.objectOrNull(): JsonObject? = try {
    asJsonObject
} catch (e: Throwable) {
    null
}

@Throws(IllegalStateException::class)
public fun JsonElement.array(): JsonArray = asJsonArray
public fun JsonElement.arrayOrNull(): JsonArray? = try {
    asJsonArray
} catch (e: Throwable) {
    null
}
