package fe.gson.extension.json.`object`

import com.google.gson.JsonObject
import fe.gson.exception.NoSuchKeyInElementFoundException
import java.math.BigDecimal
import java.math.BigInteger


@Throws(ClassCastException::class, NoSuchKeyInElementFoundException::class, AssertionError::class)
public fun JsonObject.asString(key: String): String = asPrimitive(key).asString
public fun JsonObject.asStringOrNull(key: String): String? = asPrimitiveMappedOrNull(key) { it.asString }

@Throws(ClassCastException::class, NoSuchKeyInElementFoundException::class, AssertionError::class)
public fun JsonObject.asBoolean(key: String): Boolean = asPrimitive(key).asBoolean
public fun JsonObject.asBooleanOrNull(key: String): Boolean? = asPrimitiveMappedOrNull(key) { it.asBoolean }

@Throws(ClassCastException::class, NoSuchKeyInElementFoundException::class, NumberFormatException::class)
public fun JsonObject.asInt(key: String): Int = asPrimitive(key).asInt
public fun JsonObject.asIntOrNull(key: String): Int? = asPrimitiveMappedOrNull(key) { it.asInt }

@Throws(ClassCastException::class, NoSuchKeyInElementFoundException::class, NumberFormatException::class)
public fun JsonObject.asLong(key: String): Long = asPrimitive(key).asLong
public fun JsonObject.asLongOrNull(key: String): Long? = asPrimitiveMappedOrNull(key) { it.asLong }

@Throws(ClassCastException::class, NoSuchKeyInElementFoundException::class, NumberFormatException::class)
public fun JsonObject.asDouble(key: String): Double = asPrimitive(key).asDouble
public fun JsonObject.asDoubleOrNull(key: String): Double? = asPrimitiveMappedOrNull(key) { it.asDouble }

@Throws(ClassCastException::class, NoSuchKeyInElementFoundException::class, NumberFormatException::class)
public fun JsonObject.asFloat(key: String): Float = asPrimitive(key).asFloat
public fun JsonObject.asFloatOrNull(key: String): Float? = asPrimitiveMappedOrNull(key) { it.asFloat }

@Throws(ClassCastException::class, NoSuchKeyInElementFoundException::class, NumberFormatException::class)
public fun JsonObject.asShort(key: String): Short = asPrimitive(key).asShort
public fun JsonObject.asShortOrNull(key: String): Short? = asPrimitiveMappedOrNull(key) { it.asShort }

@Throws(ClassCastException::class, NoSuchKeyInElementFoundException::class, NumberFormatException::class)
public fun JsonObject.asByte(key: String): Byte = asPrimitive(key).asByte
public fun JsonObject.asByteOrNull(key: String): Byte? = asPrimitiveMappedOrNull(key) { it.asByte }

@Throws(ClassCastException::class, NoSuchKeyInElementFoundException::class, NumberFormatException::class)
public fun JsonObject.asBigDecimal(key: String): BigDecimal = asPrimitive(key).asBigDecimal
public fun JsonObject.asBigDecimalOrNull(key: String): BigDecimal? = asPrimitiveMappedOrNull(key) { it.asBigDecimal }

@Throws(ClassCastException::class, NoSuchKeyInElementFoundException::class, NumberFormatException::class)
public fun JsonObject.asBigInteger(key: String): BigInteger = asPrimitive(key).asBigInteger
public fun JsonObject.asBigIntegerOrNull(key: String): BigInteger? = asPrimitiveMappedOrNull(key) { it.asBigInteger }