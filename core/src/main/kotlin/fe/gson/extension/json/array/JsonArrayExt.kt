package fe.gson.extension.json.array

import com.google.gson.JsonArray
import com.google.gson.JsonElement
import fe.gson.context.GlobalGsonContext

@Throws(IllegalStateException::class, AssertionError::class, NumberFormatException::class)
public inline fun <T> JsonArray.mapWithNulls(keepNulls: Boolean = true, transform: (JsonElement) -> T?): List<T?> {
    val list = ArrayList<T?>(size())
    for (element in this) {
        val transformed = transform(element)
        if (keepNulls || transformed != null) {
            list.add(transformed)
        }
    }

    return list
}

@Throws(ClassCastException::class)
public inline fun <reified T : JsonElement> JsonArray.elements(): List<T> = map { it as T }

public inline fun <reified T : JsonElement> JsonArray.elementsOrNull(
    keepNulls: Boolean = true
): List<T?> = mapWithNulls(keepNulls) { it as? T }

public operator fun JsonArray.plus(array: JsonArray): JsonArray {
    val result = JsonArray()
    result.addAll(this)
    result.addAll(array)

    return result
}

public fun JsonArray.addIfJsonElement(item: Any?): JsonArray {
    val elem = GlobalGsonContext.converter.toJsonElement(item)
    if (elem != null) add(elem)

    return this
}
