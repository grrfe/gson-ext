package fe.gson.extension

import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken

public fun JsonReader.nextStringOrNull(): String? {
    val jsonToken = peek()
    if (jsonToken == JsonToken.NULL || jsonToken != JsonToken.STRING) {
        skipValue()
        return null
    }

    return nextString()
}
