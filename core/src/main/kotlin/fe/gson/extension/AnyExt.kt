package fe.gson.extension

import com.google.gson.JsonElement
import fe.gson.context.GlobalGsonContext


public fun Any?.toJsonElement(): JsonElement? {
    return GlobalGsonContext.converter.toJsonElement(this)
}
