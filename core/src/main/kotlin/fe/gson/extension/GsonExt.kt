package fe.gson.extension

import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonIOException
import com.google.gson.JsonParseException
import com.google.gson.reflect.TypeToken
import fe.gson.type.asType
import java.io.Reader
import java.io.Writer
import java.lang.reflect.Type

@Throws(JsonParseException::class)
public inline fun <reified T> Gson.readJson(reader: Reader, typeToken: TypeToken<T>): T {
    return this.fromJson(reader, typeToken)
}

@Throws(JsonParseException::class)
public inline fun <reified T> Gson.readJson(reader: Reader, type: Type = asType<T>()): T {
    return this.fromJson(reader, type)
}

@Throws(JsonIOException::class)
public inline fun <reified T : JsonElement> Gson.writeJson(jsonElement: T, writer: Writer) {
    this.toJson(jsonElement, writer)
}

@Throws(JsonIOException::class)
public fun Gson.writeJson(src: Any, writer: Writer) {
    this.toJson(src, writer)
}
