package fe.gson.type

import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import kotlin.jvm.java
import kotlin.reflect.KClass


public inline fun <reified T> asTypeToken(): TypeToken<T> {
    return object : TypeToken<T>() {}
}

public inline fun <reified T> asType(): Type {
    return asTypeToken<T>().type
}

public inline fun <reified T> TypeToken<T>.asList(): TypeToken<List<T>> {
    @Suppress("UNCHECKED_CAST")
    return wrapIn(List::class) as TypeToken<List<T>>
}

public inline fun <Wrapper : Any, reified T> TypeToken<T>.wrapIn(clazz: KClass<Wrapper>): TypeToken<Wrapper> {
    @Suppress("UNCHECKED_CAST")
    return TypeToken.getParameterized(clazz.java, type) as TypeToken<Wrapper>
}
