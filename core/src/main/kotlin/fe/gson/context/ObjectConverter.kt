package fe.gson.context

import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonNull
import com.google.gson.JsonObject
import com.google.gson.JsonPrimitive

public abstract class ObjectConverter(
    useJsonNull: Boolean = true,
    protected val enableJsonArrayConversion: Boolean = true,
    protected val enableJsonObjectConversion: Boolean = true,
) {
    protected val jsonNull: JsonNull? = if (useJsonNull) JsonNull.INSTANCE else null

    public abstract fun toJsonElement(any: Any?): JsonElement?

    public open fun toJsonPrimitiveOrNull(any: Any): JsonPrimitive? {
        return when (any) {
            is String -> JsonPrimitive(any)
            is Boolean -> JsonPrimitive(any)
            is Char -> JsonPrimitive(any)
            is Number -> JsonPrimitive(any)
            else -> null
        }
    }

    public open fun toJsonArrayOrNull(any: Any): JsonArray? {
        if (any is Iterable<*>) {
            val array = JsonArray()
            for (item in any) array.add(toJsonElement(item))

            return array
        }

        if (any is Array<*>) {
            val array = JsonArray()
            for (item in any) array.add(toJsonElement(item))

            return array
        }

        return null
    }

    public open fun toJsonObjectOrNull(map: Map<*, *>): JsonObject {
        val obj = JsonObject()
        for ((key, value) in map) {
            if (key is String) obj.add(key, toJsonElement(value))
        }

        return obj
    }

    public open fun toJsonTree(any: Any?): JsonElement? {
        return GlobalGsonContext.gson.toJsonTree(any)
    }

    public companion object Default : ObjectConverter() {
        override fun toJsonElement(any: Any?): JsonElement? {
            if (any == null) return jsonNull
            if (any is IJsonElement) return any.toJsonElement()
            if (any is String || any is Boolean || any is Char || any is Number) {
                return toJsonPrimitiveOrNull(any)
            }

            if (enableJsonArrayConversion && any !is JsonArray && (any is Iterable<*> || any is Array<*>)) {
                return toJsonArrayOrNull(any)
            }

            if (enableJsonObjectConversion && any is Map<*, *>) {
                return toJsonObjectOrNull(any)
            }

            return toJsonTree(any)
        }
    }
}
