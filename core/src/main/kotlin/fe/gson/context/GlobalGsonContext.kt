package fe.gson.context

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import fe.gson.typeadapter.ExtendedTypeAdapter


public typealias ConfigureGson = GsonBuilder.() -> Unit

@Suppress("MemberVisibilityCanBePrivate")
public object GlobalGsonContext {
    public val defaultConfiguration: ConfigureGson = {
        setPrettyPrinting()
        disableHtmlEscaping()
        ExtendedTypeAdapter.register(this)
    }

    private var instance: Gson? = null
    public var converter: ObjectConverter = ObjectConverter.Default
        private set

    public val gson: Gson
        get() {
            if (instance == null) configure(extendDefaultGsonConfig = false, configure = defaultConfiguration)
            return instance!!
        }

    public fun configure(
        converter: ObjectConverter = ObjectConverter.Default,
        extendDefaultGsonConfig: Boolean = true,
        configure: ConfigureGson,
    ): GlobalGsonContext {
        if (instance == null) {
            val gsonBuilder = GsonBuilder()
            val default = if (extendDefaultGsonConfig) gsonBuilder.apply(defaultConfiguration) else gsonBuilder
            val gson = default.apply(configure).create()

            this.instance = gson
            this.converter = converter
        }

        return this
    }
}
