package fe.gson.context

import com.google.gson.JsonElement

public interface IJsonElement {
    public fun toJsonElement(): JsonElement
}
