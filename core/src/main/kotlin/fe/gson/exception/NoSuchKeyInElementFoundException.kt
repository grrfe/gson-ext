package fe.gson.exception

import com.google.gson.JsonElement

public class NoSuchKeyInElementFoundException(
    element: JsonElement,
    key: String
) : Exception("$element does not have a key $key")

public fun noSuchKeyException(element: JsonElement, key: String): Nothing = throw NoSuchKeyInElementFoundException(element, key)
