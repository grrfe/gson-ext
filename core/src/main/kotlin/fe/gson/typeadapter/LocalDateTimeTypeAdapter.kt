package fe.gson.typeadapter

import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import fe.gson.extension.nextStringOrNull
import java.io.IOException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

public open class LocalDateTimeTypeAdapter(
    private val formatter: DateTimeFormatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME
) : ExtendedTypeAdapter<LocalDateTime>(LocalDateTime::class.java) {

    @Throws(IOException::class)
    override fun write(writer: JsonWriter, date: LocalDateTime?) {
        writer.value(date?.format(formatter))
    }

    @Throws(IOException::class)
    override fun read(reader: JsonReader): LocalDateTime? {
        return reader.nextStringOrNull()?.let { LocalDateTime.parse(it, formatter) }
    }

    public companion object Default : LocalDateTimeTypeAdapter()
}
