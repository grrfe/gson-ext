package fe.gson.typeadapter

import com.google.gson.GsonBuilder
import com.google.gson.TypeAdapter

public abstract class ExtendedTypeAdapter<T : Any>(private val clazz: Class<T>) : TypeAdapter<T>() {
    public fun register(builder: GsonBuilder) {
        builder.registerTypeAdapter(clazz, this)
    }

    public companion object Default {
        @Suppress("MemberVisibilityCanBePrivate")
        public val dateTime: Set<ExtendedTypeAdapter<out Any>> = setOf(
            LocalDateTimeTypeAdapter.Default,
            LocalDateTypeAdapter.Default,
            LocalTimeTypeAdapter.Default,
            OffsetDateTimeTypeAdapter.Default
        )

        public fun register(builder: GsonBuilder) {
            dateTime.register(builder)
        }
    }
}

public fun Iterable<ExtendedTypeAdapter<*>>.register(builder: GsonBuilder) {
    for (adapter in this) adapter.register(builder)
}
