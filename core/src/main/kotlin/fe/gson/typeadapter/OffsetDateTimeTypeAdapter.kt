package fe.gson.typeadapter

import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import fe.gson.extension.nextStringOrNull
import java.io.IOException
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter

public open class OffsetDateTimeTypeAdapter(
    private val formatter: DateTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME,
) : ExtendedTypeAdapter<OffsetDateTime>(OffsetDateTime::class.java) {

    @Throws(IOException::class)
    override fun write(writer: JsonWriter, date: OffsetDateTime?) {
        writer.value(date?.format(formatter))
    }

    @Throws(IOException::class)
    override fun read(reader: JsonReader): OffsetDateTime? {
        return reader.nextStringOrNull()?.let { OffsetDateTime.parse(it, formatter) }
    }

    public companion object Default : OffsetDateTimeTypeAdapter()
}
