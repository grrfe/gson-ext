package fe.gson

import com.google.gson.Gson
import com.google.gson.JsonParseException
import com.google.gson.reflect.TypeToken
import fe.gson.context.GlobalGsonContext
import fe.gson.extension.readJson
import fe.gson.type.asTypeToken
import java.io.Reader

public class JsonReader {
    @Throws(JsonParseException::class, ClassCastException::class)
    public inline fun <reified T> fromJson(
        reader: Reader,
        gson: Gson = GlobalGsonContext.gson,
        typeToken: TypeToken<T> = asTypeToken<T>(),
    ): T {
        return reader.use { gson.readJson<T>(it) }
    }

    @Throws(JsonParseException::class, ClassCastException::class)
    public inline fun <reified T> fromJson(
        string: String,
        gson: Gson = GlobalGsonContext.gson,
        typeToken: TypeToken<T> = asTypeToken<T>(),
    ): T {
        return fromJson(string.reader(), gson, typeToken)
    }

    public inline fun <reified T> fromJsonOrNull(
        reader: Reader,
        gson: Gson = GlobalGsonContext.gson,
        typeToken: TypeToken<T> = asTypeToken<T>(),
    ): T? {
        return runCatching { fromJson<T>(reader, gson, typeToken) }.getOrNull()
    }

    public inline fun <reified T> fromJsonOrNull(
        string: String,
        gson: Gson = GlobalGsonContext.gson,
        typeToken: TypeToken<T> = asTypeToken<T>(),
    ): T? {
        return fromJsonOrNull(string.reader(), gson, typeToken)
    }
}
