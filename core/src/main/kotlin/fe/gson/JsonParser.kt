package fe.gson

import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import com.google.gson.JsonParser
import java.io.Reader

public class JsonParser {
    @Throws(JsonParseException::class, ClassCastException::class)
    public inline fun <reified T : JsonElement> parseJson(reader: Reader): T {
        return reader.use { JsonParser.parseReader(it) as T }
    }

    @Throws(JsonParseException::class, ClassCastException::class)
    public inline fun <reified T : JsonElement> parseJson(string: String): T {
        return parseJson<T>(string.reader())
    }

    public inline fun <reified T : JsonElement> parseJsonOrNull(reader: Reader): T? {
        return runCatching { parseJson<T>(reader) }.getOrNull()
    }

    public inline fun <reified T : JsonElement> parseJsonOrNull(string: String): T? {
        return parseJsonOrNull(string.reader())
    }
}
