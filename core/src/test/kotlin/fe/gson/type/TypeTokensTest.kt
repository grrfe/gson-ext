package fe.gson.type

import assertk.assertThat
import assertk.assertions.containsOnly
import com.google.gson.Gson
import fe.gson.extension.readJson
import kotlin.test.Test

internal class TypeTokensTest {
    @Test
    fun `nested parameterized type test`() {
        data class Test(val value: String)
        data class TestWrapper<T>(val wrapper: String, val data: T)

        val json = """[
            |{
            |  "wrapper": "hello", 
            |  "data": {
            |    "value": "world"
            |  }
            |},
            |{
            |  "wrapper": "foo", 
            |  "data": {
            |    "value": "bar"
            |  }
            |}
            |]""".trimMargin()

        val typeToken = asTypeToken<TestWrapper<Test>>().asList()
        val result = Gson().readJson(json.reader(), typeToken)

        assertThat(result).containsOnly(
            TestWrapper("hello", Test("world")),
            TestWrapper("foo", Test("bar")),
        )
    }
}
