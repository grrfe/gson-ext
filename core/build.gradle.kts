import fe.buildsrc.Dependencies

plugins {
    kotlin("jvm")
    `java-library`
    `maven-publish`
    id("net.nemerosa.versioning")
}

dependencies {
    api("com.google.code.gson:gson:_")
    testImplementation(kotlin("test"))
    testImplementation("com.willowtreeapps.assertk:assertk:0.28.1")
}

kotlin {
    jvmToolchain(17)
    explicitApi()
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = project.group.toString()
            artifactId = "core"
            version = Dependencies.with(versioning, Dependencies.gson)

            from(components["java"])
        }
    }
}
