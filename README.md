# gson-ext

Kotlin extension function and utilities for GSON (Google's Java Json Library)

## Usable via
[![](https://jitpack.io/v/com.gitlab.grrfe/gson-ext.svg)](https://jitpack.io/#com.gitlab.grrfe/gson-ext)
