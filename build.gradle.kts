import fe.buildsrc.Dependencies

plugins {
    kotlin("jvm") apply false
    java
    id("net.nemerosa.versioning")
}

allprojects {
    apply(plugin = "net.nemerosa.versioning")

    group = "fe.gson-ext"
    version = Dependencies.with(versioning)

    repositories {
        mavenCentral()
        maven(url = "https://jitpack.io")
    }
}
